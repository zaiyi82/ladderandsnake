package gamelib

import (
	"fmt"
	"math/rand"
)

// Map contains the size and the ladders/snakes on the map
type Map struct {
	Steps     int
	Shortcuts []Shortcut
}

// Shortcut is either a ladder or a snake on the map
type Shortcut struct {
	Type          string
	StartPosition int
	EndPosition   int
}

// Initialize the map to a given number of steps, num of ladders & shnakes
func (aMap *Map) Initialize(numSteps int, numLadders int, numSnakes int) {
	aMap.Steps = numSteps

	// boolean array to hold the flag
	shortcutsAreLadders := make([]bool, numLadders+numSnakes)
	for i := 0; i < numLadders; i++ {
		shortcutsAreLadders[i] = true
	}
	for i := 0; i < numSnakes; i++ {
		shortcutsAreLadders[i+numLadders] = false
	}

	for _, isLadder := range shortcutsAreLadders {
		for {
			shortcut := generateShortcut(numSteps, isLadder)
			isValidShortcut := validateShortcut(shortcut, aMap.Shortcuts)
			if isValidShortcut {
				aMap.Shortcuts = append(aMap.Shortcuts, shortcut)
				break // break and continue with the next shortcut
			}
		}
	}
}

func generateShortcut(numSteps int, isLadder bool) Shortcut {
	// valid numbers for both: 1...numSteps-1
	// for the smaller one:  1...numSteps-2
	// for the lager one: 2...numSteps-1
	smallPosition := rand.Intn(numSteps-2) + 1 // [1...numSteps-2], both inclusive
	largePosition := rand.Intn(numSteps-smallPosition-1) + smallPosition + 1

	if isLadder {
		return Shortcut{
			Type:          "ladder",
			StartPosition: smallPosition,
			EndPosition:   largePosition,
		}
	} else {
		return Shortcut{
			Type:          "snake",
			StartPosition: largePosition,
			EndPosition:   smallPosition,
		}
	}
}

func validateShortcut(newShortcut Shortcut, existingShortcuts []Shortcut) bool {
	for _, existingShortcut := range existingShortcuts {
		if existingShortcut.StartPosition == newShortcut.StartPosition {
			return false
		}
	}
	// reach here: no existing shortcuts are having the same start pos as the new shortcut. ok to use
	return true
}

// Print the map on screen
func (aMap *Map) Print() {

	// ladders on the top, snakes at the bottom
	// each shortcut takes one line. total # of lines: 1 row for map + ladders + snakes
	// each number takes 2 positions. prefixed with 0.  arrows pointing at the 2nd digit of the position
	for i := 0; i <= aMap.Steps; i++ {
		if i < 10 {
			fmt.Printf(".0%d", i)
		} else {
			fmt.Printf(".%d", i)
		}
	}
	fmt.Printf("\n")

	shortcutMap := [][]string{}
	for i := 0; i < len(aMap.Shortcuts); i++ {
		pathRow := make([]string, aMap.Steps)
		arrowRow := make([]string, aMap.Steps)
		shortcutMap = append(shortcutMap, arrowRow)
		shortcutMap = append(shortcutMap, pathRow)

		shortcut := aMap.Shortcuts[i]
		// print the start position
		arrowRow[shortcut.StartPosition] = "|"
		pathRow[shortcut.StartPosition] = "|"
		// print the end position
		arrowRow[shortcut.EndPosition] = "^"
		pathRow[shortcut.EndPosition] = "|"

		// print the line
		increment := 1
		if shortcut.Type == "snake" {
			increment = -1
		}
		for j := shortcut.StartPosition + increment; j != shortcut.EndPosition; j += increment {
			pathRow[j] = "-"
		}
	}

	for i := 0; i < len(shortcutMap); i++ {
		for j := 0; j < len(shortcutMap[i]); j++ {
			if shortcutMap[i][j] == "-" {
				fmt.Printf("---")
			} else if shortcutMap[i][j] == "" {
				fmt.Printf(".  ")
			} else {
				fmt.Printf(".%s ", shortcutMap[i][j])
			}
		}
		// last step
		fmt.Printf(".  .")
		if i%2 == 1 {
			shortcut := aMap.Shortcuts[(i-1)/2]
			fmt.Printf("%s (%d, %d)", shortcut.Type, shortcut.StartPosition, shortcut.EndPosition)
		}
		fmt.Printf("\n")
	}
}
