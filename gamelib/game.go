package gamelib

import (
	"fmt"
)

// Game contains a number of players and a map
type Game struct {
	Players []Player
	Map     Map
}

// Initialize a new game
func (game *Game) Initialize(numPlayers int, aMap Map) {
	game.Players = make([]Player, numPlayers)
	game.Map = aMap
}

// Print the status of the game, where the player is on the map
func (game *Game) Print() {
	for i := 0; i < len(game.Players); i++ {
		player := game.Players[i]
		playerRow := make([]string, game.Map.Steps*3)
		playerRow[player.Position*3+1] = "P"
		playerRow[player.Position*3+2] = fmt.Sprintf("%d", i+1)
		for j := 0; j < (game.Map.Steps+1)*3; j++ {
			if j == player.Position*3+1 {
				fmt.Print("P")
			} else if j == player.Position*3+2 {
				fmt.Print(i + 1)
			} else if j%3 == 0 {
				fmt.Print(".")
			} else {
				fmt.Print(" ")
			}
		}
		fmt.Println()
	}
	game.Map.Print()
}
