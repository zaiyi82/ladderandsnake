package gamelib

import (
	"fmt"
	"math/rand"
)

// Player represents one player
type Player struct {
	Position int
}

// PlayerMove represents one uni-directional move of a player
type PlayerMove struct {
	Description   string
	StartPosition int
	EndPosition   int
	Increment     int // +1, -1, or EndPos - StartPos (direct jump, e.g., through ladder/snake)
}

// MoveOnMap moves the player on the map, observing snakes and ladders, and return the digest of the moves taken
func (player *Player) MoveOnMap(aMap Map) []PlayerMove {

	moves := []PlayerMove{}

	// 1. throw dice: random number 1-6.  a move that does not actaully move yet.
	diceVal := rand.Intn(6) + 1
	moves = append(moves, PlayerMove{
		Description:   fmt.Sprintf("throw dice: %d", diceVal),
		StartPosition: player.Position,
		EndPosition:   player.Position,
		Increment:     0,
	})

	// 2. advance by dice value
	newPos := player.Position + diceVal

	// 3. check for overshoot
	if newPos > aMap.Steps {
		retreatSteps := newPos - aMap.Steps
		newPos = aMap.Steps - retreatSteps
		// move to destination, and move back
		moves = append(moves, PlayerMove{
			Description:   "move forward",
			StartPosition: player.Position,
			EndPosition:   aMap.Steps,
			Increment:     1,
		})
		moves = append(moves, PlayerMove{
			Description:   "retreat back",
			StartPosition: aMap.Steps,
			EndPosition:   newPos,
			Increment:     -1,
		})
	} else {
		// no retreat needed, directly move forward
		moves = append(moves, PlayerMove{
			Description:   "move forward",
			StartPosition: player.Position,
			EndPosition:   newPos,
			Increment:     1,
		})
	}

	// 4. check for snakes & ladders, max apply once.
	for _, shortcut := range aMap.Shortcuts {
		description := "climb ladder"
		if shortcut.Type == "snake" {
			description = "kenna snake"
		}
		if shortcut.StartPosition == newPos {
			newPos = shortcut.EndPosition
			moves = append(moves, PlayerMove{
				Description:   description,
				StartPosition: shortcut.StartPosition,
				EndPosition:   shortcut.EndPosition,
				Increment:     shortcut.EndPosition - shortcut.StartPosition,
			})
			break
		}
	}

	player.Position = newPos
	return moves
}

func makeMovesBy(start int, steps int, forward bool) []int {
	increment := 1
	if !forward {
		increment = -1
	}

	moves := []int{}
	for i := 0; i < steps; i++ {
		moves = append(moves, start+i*increment)
	}
	return moves
}

func makeMovesTo(start int, end int) []int {
	increment := 1
	if end < start {
		increment = -1
	}

	moves := []int{}
	for pos := start; pos != end; pos += increment {
		moves = append(moves, pos)
	}
	return moves
}
