package main

import (
	"flag"
	"fmt"
	"ladderandsnake/gamelib"
	"strconv"
	"time"
)

// example command line:
// go run main.go -steps=20 -ladders=3 -snakes=2 -players=3
func main() {

	numStepsPtr := flag.Int("steps", 30, "number of steps in the map")
	numLaddersPtr := flag.Int("ladders", 5, "number of ladders in the map")
	numSnakesPtr := flag.Int("snakes", 5, "number of snakes in the map")
	numPlayersPtr := flag.Int("players", 2, "number of players")
	flag.Parse()

	aMap := gamelib.Map{}
	aMap.Initialize(*numStepsPtr, *numLaddersPtr, *numSnakesPtr)
	game := gamelib.Game{}
	game.Initialize(*numPlayersPtr, aMap)
	gameUI := newGameUI(game)

	gameUI.renderGame()

OuterLoop:
	for {
		for playerIndex := 0; playerIndex < len(game.Players); playerIndex++ {

			moves := gameUI.Players[playerIndex].MoveOnMap(aMap)
			gameUI.renderMoves(playerIndex, moves)
			renderPause()
			if game.Players[playerIndex].Position == game.Map.Steps {
				gameUI.renderStatus(fmt.Sprintf("P%d has won.", playerIndex))
				moveCursorTo(gameUI.Height)
				break OuterLoop
			}
		}
	}
}

// GameUI, rendering stuff --------------------------------------------------------

// GameUI is a wrapper of Game
type GameUI struct {
	gamelib.Game
	Width  int
	Height int
}

func newGameUI(game gamelib.Game) GameUI {
	width := 3 * (game.Map.Steps + 1)
	height := 1 + len(game.Players) + 1 + 2*len(game.Map.Shortcuts) // 1 line of status, players, 1 line of map, shortcuts
	return GameUI{game, width, height}
}

func (gameUI *GameUI) renderGame() {

	// clear the screen
	moveCursorTo(1)
	emptyLine := string(newByteArray(gameUI.Width, ' '))
	for i := 0; i < gameUI.Height; i++ {
		fmt.Println(emptyLine)
	}

	// print the game
	moveCursorTo(2)
	for i, player := range gameUI.Players {
		gameUI.renderPlayerAt(i, player.Position)
	}
	gameUI.Map.Print()
}

func (gameUI *GameUI) renderStatus(statusMsg string) {
	moveCursorTo(1)
	fmt.Print(string(newByteArray(gameUI.Width, ' ')))
	moveCursorTo(1)
	fmt.Print(string(statusMsg))
}

func (gameUI *GameUI) renderMoves(playerIndex int, moves []gamelib.PlayerMove) {

	for _, move := range moves {

		// print status line
		statusMsg := fmt.Sprintf("P%d %s: %d -> %d", playerIndex, move.Description, move.StartPosition, move.EndPosition)
		if move.StartPosition == move.EndPosition {
			statusMsg = fmt.Sprintf("P%d %s", playerIndex, move.Description)
		}
		gameUI.renderStatus(statusMsg)

		// interpolate movement steps
		moveSteps := []int{}
		for pos := move.StartPosition; pos != move.EndPosition; pos += move.Increment {
			moveSteps = append(moveSteps, pos)
		}
		// last step to reach endpos
		moveSteps = append(moveSteps, move.EndPosition)

		for _, pos := range moveSteps {
			// fmt.Printf("P%d to: %d\n", playerIndex, pos)
			gameUI.renderPlayerAt(playerIndex, pos)
			renderPause()
		}
	}
}

func (gameUI *GameUI) renderPlayerAt(playerIndex int, playerPosition int) {

	moveCursorTo(playerIndex + 2)

	playerLineBytes := newByteArray(gameUI.Width, '=')
	playerName := []byte("P" + strconv.Itoa(playerIndex))
	playerLineBytes[3*playerPosition+1] = playerName[0]
	playerLineBytes[3*playerPosition+2] = playerName[1]

	fmt.Println(string(playerLineBytes))
}

// utils -------------------------------------------------------------------

func moveCursorTo(lineNum int) {
	fmt.Printf("\033[%d;0H", lineNum) // top, left
}

func newByteArray(length int, char byte) []byte {
	byteArray := make([]byte, length)
	for i := range byteArray {
		byteArray[i] = char
	}
	return byteArray
}

func renderPause() {
	time.Sleep(100 * time.Millisecond)
}
